FROM tensorflow/tensorflow:2.1.2-gpu-jupyter

ARG PIP_TIMEOUT=1000

RUN set -euo pipefail; \
  set -x; \
  mkdir -p /.cache/pip; \
  find /.cache/ -type d -exec chmod 777 {} \;; \
  find /.cache/ -type f -exec chmod 666 {} \;; \
  mkdir -p /tf/{assets,lib,notebooks,requirements,run}; \
  mkdir -p /tf/run/{shared,notebooks}; \
  mkdir -p /tf/run/{checkpoints,generated,logs}

# TODO: this ugly hack allows running e.g. `pip install` on Notebooks for
# containers started with `-u` option (run as local user).
RUN chmod -R 777 /usr/local/

# Install dependencies.

RUN set -euo pipefail; \
  set -x; \
  apt-get install -y \
    libsm6 \
    libxrender1 \
    libfontconfig1 \
    libxext6 \
    graphviz; \
  apt-get clean; rm -rf /var/lib/apt/lists/

COPY jupyter-data_science-gpu/requirements.txt /tf/requirements/

RUN chmod -R 777 /tf

RUN set -euo pipefail; \
  set -x; \
  pip install --default-timeout="${PIP_TIMEOUT}" -r /tf/requirements/requirements.txt

CMD ["bash", "-c", "source /etc/bash.bashrc && jupyter notebook --ip '0.0.0.0' --no-browser --NotebookApp.token='' --NotebookApp.password='' --notebook-dir='/tf' --allow-root"]
